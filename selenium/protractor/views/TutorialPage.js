var TutorialPage =  function() {

var tutorialHeader = element(by.css('#navbar-sub > div > div.grid-right > ul > li > a'));

  var url = 'https://docs.angularjs.org/tutorial';

  this.get = function() {
      browser.get(url);
  };
  this.getHeaderText = function () {
      return tutorialHeader.getText();
  }
};
module.exports = new TutorialPage();