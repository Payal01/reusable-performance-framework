var AngularHomePage = function() {
  var nameInput = element(by.model('yourName'));
  var greeting = element(by.binding('yourName'));
  var learn = element(by.cssContainingText('[href*="#"]', 'Learn'));
  var tutorial = element(by.cssContainingText('[href*="https://docs.angularjs.org/tutorial"]', 'Tutorial'));

  var url = 'http://www.angularjs.org';

  this.get = function() {
    browser.get(url);
  };

  this.setName = function(name) {
    nameInput.sendKeys(name);
  };

  this.getGreetingText = function() {
    return greeting.getText();
  };

  this.learnMenuClick = function () {
    learn.click();
  }

  this.tutorialMenuClick = function () {
    tutorial.click();
  }
};
module.exports = new AngularHomePage();