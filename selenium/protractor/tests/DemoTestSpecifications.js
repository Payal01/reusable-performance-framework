var angularHomePage = require('../views/AngularHomePage');
var tutorialPage = require ('../views/TutorialPage');
var user = require('../models/User');
var helper = require ('../helpers/TestHelper');

describe('angularjs homepage', function() {
  it('should greet the named user', function() {
    user.setName (helper.getRandomString (10));
    angularHomePage.get();
    angularHomePage.setName(user.getName());
    expect(angularHomePage.getGreetingText()).toEqual('Hello ' + user.getName() + '!');
  });
});

describe ('angularjs homepage', function() {
it ('should navigate to the Tutorial page on Tutorial click', function () {
    angularHomePage.get();
    angularHomePage.learnMenuClick();
    angularHomePage.tutorialMenuClick();
    browser.sleep(1000);
    expect (tutorialPage.getHeaderText()).toContain('Tutorial');
  });
});

