var TestHelper = function() {

  this.getRandomString = function(length) {
  var string = '';
  var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
          for (i = 0; i < length; i++) {
              string += letters.charAt(Math.floor(Math.random() * letters.length));
          }
          return string;
      };
};
module.exports = new TestHelper();