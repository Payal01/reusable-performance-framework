# README #

This README would normally document whatever steps are necessary to get your performance test up and running.

### What is this repository for? ###

#### Quick summary
The reusable performance framework is a plug and use framework that can adapt itself
to most of test executor. No more need to re-write your performance test, 
this framework helps leverage your current test to a performance test.
* Version 1.0
* clone the [Reusable Framework] from the Repository `https://bitbucket.org/Payal01/reusable-performance-framework/src/master/`
### Accesses
- Repository
- [x] Payal Fnu Kumari `fnu.payalkumari@slalom.com`

### How do I get set up? ###
####Summary of set up
Keep in mind that some additional software can be required depend of test type.
##1. Configuration
###Windows
- [x] Install Taurus system dependencies
* Get [Python 3.7+](http://www.python.org/downloads) and install it, don't forget to enable "Add python.exe to Path" checkbox.
* Get [latest](https://www.java.com/download/) Java  and install it.


- [x] Installing Taurus With Prebuilt Installer
* Download an [installer](https://gettaurus.org/builds/TaurusInstaller_1.14.2_x64.exe) 
####Notes:

You can use /S option of the installer to perform silent installation.
After the installation is finished, you can run Taurus with bzt from command prompt. Also, you'll have 'Taurus' shortcut in the Start menu.

- [x] Installing Taurus Manually
1. `pip install --upgrade pip wheel`
2. `pip install bzt`
3. `pip install --upgrade bzt`

###Mac OS
- [x] Install
- `brew install bzt`
- [x] update 
- `brew upgrade bzt`

###Lunix
You will need Python 3.6+ and Java installed. 
- [x] install Python 3.6+ and Java 
- `sudo apt-get update
    sudo apt-get install python3 default-jre-headless python3-tk python3-pip python3-dev \
      libxml2-dev libxslt-dev zlib1g-dev net-tools
    sudo python3 -m pip install bzt`
- [x] Upgrading to latest
- `sudo python3 -m pip install --upgrade bzt`
- [x] Install Taurus
- `sudo python3 -m pip install --upgrade bzt`
- [ ] Upgrade
- `pip install --upgrade bzt`

###Docker Image
To use it, create a directory, for example /tmp/my-test, put all configs and additional files like JMXses there, then start Docker like this:
- `docker run -it --rm -v /tmp/my-test:/bzt-configs blazemeter/taurus my-config.yml`
- [x] Make note that /tmp/my-test was passed in -v Docker option, it's crucial.
1. Directory /tmp/my-test is mounted as /bzt-configs
2. Current directory changed to /bzt-configs
3. Taurus is started with the config files you specified: `bzt /bzt-configs/my-config.yml

- [ ] You can also specify multile config files in the docker run command with wildcards or as separate arguments like so:
- `docker run -it --rm -v /tmp/my-test:/bzt-configs blazemeter/taurus *.yml`
- `docker run -it --rm -v /tmp/my-test:/bzt-configs blazemeter/taurus my-config-1.json my-config-2.json`
#Run Tests
Using the command line too:
---------------------------
- Go to module (i.e: Selenium)
- Go to root of the framework suitable for your project (i.e maven). this is where there will be your .yml file.
- run bzt NAME_OF_YML.yml (i.e: `bzt smock_test.yml`)
###Additional Taurus Command-Line Options
You can still pass command-line options to Taurus through the Docker image
- `docker run -it --rm -v /tmp/my-test:/bzt-configs blazemeter/taurus my-config-1.yml -o scenarios.sample.data-sources.0=data.csv`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
- [x] ATL QA 
* Other community or team contact
- [x] Slalom Leadership
#Need Help to configure your test ?
All contributor can help to setup and demo this framework.
However, we would encourage you to get in touch with our specialist per module.
####Javascript module
- [x] Nataliya Peshekhodko `nataliya.peshekhodko@slalom.com`
####Java module
- [x] Grace Tshihata `grace.tshihata@slalom.com`
- [x] Charles Lee `charles.lee@slalom.com`
- [x] Michael Salmon `michael.salmon@slalom.com`
####Jmeter module
- [x] Grace Tshihata `grace.tshihata@slalom.com`
- [ ] Payal Kumari `fnu.payalkumari@slalom.com`
####Docker & Virtualization
- [x] Nataliya Peshekhodko `nataliya.peshekhodko@slalom.com`
- [x] Grace Tshihata `grace.tshihata@slalom.com`
- [x] Charles Lee `charles.lee@slalom.com`
- [ ] Payal Kumari `fnu.payalkumari@slalom.com`
####Documentation
- [ ] Payal Kumari `fnu.payalkumari@slalom.com`
- [x] Tom English `tom.english@slalom.com`
#### Other General Specialist
- [ ] Payal Kumari `fnu.payalkumari@slalom.com`
- [ ] Mit Raval `mit.raval@slalom.com`
- [ ] Darron Biles `darron.biles@slalom.com`
- [ ] Tom English `tom.english@slalom.com`
#####Credit https://gettaurus.org